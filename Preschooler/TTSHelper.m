//
//  TTSHelper.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 18/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "TTSHelper.h"

@interface TTSHelper()
-(void)speakText:(NSString *)text rate:(float)rate;
@end

@implementation TTSHelper
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self) {
        voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-GB"];
        speech = [[AVSpeechSynthesizer alloc] init];
        speech.delegate = self;
    }
    return self;
}

-(void)speakPrompt:(NSString *)text
{
    [self speakText:text rate:0.15];
}

-(void)speakAnswer:(NSString *)text
{
    [self speakText:text rate:0.18];
}

-(void)speakText:(NSString *)text rate:(float)rate
{
    AVSpeechUtterance *welcome = [[AVSpeechUtterance alloc] initWithString:text];
    welcome.rate = rate;
    welcome.voice = voice;
    welcome.volume = 0.6F;
    [speech speakUtterance:welcome];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    if(utterance.rate==0.15F)
    {
        [delegate promptCompleted];
    }
    else
    {
        [delegate answerCompleted];
    }
}

@end
