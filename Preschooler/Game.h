//
//  Game.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quiz.h"

static int const kChoiceCount = 6;

enum GameType
{
    GTColors,
    GTShapes,
    GTBigLetters,
    GTSmallLetters,
    GTNumbers,
    GTObjects
};

@interface Game : NSObject
{
    NSArray *_data;
    NSArray *_names;
    NSMutableArray *_indices;
    NSArray *_choiceNumbers;
}
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) int points;
@property (assign, nonatomic) NSUInteger progress;
@property (assign, nonatomic) NSUInteger total;
@property enum GameType gameType;
@property (strong, nonatomic) Quiz* quiz;
@property (readonly) BOOL over;
@property (readonly) int rank;
@property (assign, nonatomic) BOOL paused;
-(void)getQuiz;
-(void)reset;
-(void)getData;
@end
