//
//  GameRank.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "GameRank.h"

@implementation GameRank
-(id)initWithName:(NSString *)name andPoints:(int)points andRank:(int)rank
{
    self = [super init];
    if(self!=nil)
    {
        self.name = name;
        self.points = points;
        self.rank = rank;
    }
    return self;
}
@end
