//
//  Quiz.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Quiz.h"

@implementation WordChoice

-(id)initWithName:(NSString *)name andWord:(NSString *)word
{
    self = [super init];
    if(self!=nil)
    {
        self.name = name;
        self.word = word;
    }
    return self;
}

@end

@implementation ImageChoice

-(id)initWithName:(NSString *)name andImage:(UIImage *)image
{
    self = [super init];
    if(self!=nil)
    {
        self.name = name;
        self.image = image;
    }
    return self;
}

@end

@implementation Quiz

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.attempts = 0;
        self.answered = NO;
    }
    return self;
}

@end
