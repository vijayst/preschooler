//
//  ViewController.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 25/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController
{

}
-(void)showLandscape;
-(void)showPortrait;
-(NSArray *)getGameRank;
- (IBAction)clickColor:(id)sender;
- (IBAction)clickNumber:(id)sender;
- (IBAction)clickAlpha:(id)sender;
- (IBAction)clickSmall:(id)sender;
- (IBAction)clickShape:(id)sender;
- (IBAction)clickObject:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *shapeButton;
@property (strong, nonatomic) IBOutlet UIButton *alphaButton;
@property (strong, nonatomic) IBOutlet UIButton *colorButton;
@property (strong, nonatomic) IBOutlet UIButton *smallButton;
@property (strong, nonatomic) IBOutlet UIButton *numberButton;
@property (strong, nonatomic) IBOutlet UIButton *objectButton;
@property (strong, nonatomic) UIViewController *achievementController;
@end
