//
//  ProgressBar.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 02/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ProgressBar.h"

@implementation ProgressBar
@synthesize fillColor;
@synthesize backColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.fillColor = [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1];
        self.backColor = [UIColor yellowColor];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.fillColor = [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1];
        self.backColor = [UIColor colorWithRed:1 green:1 blue:0.5 alpha:1];
    }
    return self;
}

-(void)setProgress:(float)pValue
{
    value = pValue;
    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    float width = rect.size.width*value;
    float height = rect.size.height;
    CGRect rectangle = CGRectMake(0, 0, width, height);
    CGContextSetFillColorWithColor(context, fillColor.CGColor);
    CGContextFillRect(context, rectangle);
    CGRect backRectangle = CGRectMake(width, 0, rect.size.width-width, height);
    CGContextSetFillColorWithColor(context, backColor.CGColor);
    CGContextFillRect(context, backRectangle);
}


@end
