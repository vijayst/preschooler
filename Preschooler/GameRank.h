//
//  GameRank.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameRank : NSObject
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) int points;
@property (assign, nonatomic) int rank;
-(id)initWithName:(NSString *)name andPoints:(int)points andRank:(int)rank;
@end
