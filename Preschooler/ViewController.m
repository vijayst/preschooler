//
//  ViewController.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 25/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ViewController.h"
#import "AudioHelper.h"
#import "ColorViewController.h"
#import "ColorGame.h"
#import "NumberGame.h"
#import "BigLetterGame.h"
#import "SmallLetterGame.h"
#import "ObjectGame.h"
#import "ShapeGame.h"
#import "GameRank.h"

@interface ViewController ()
@property (strong, nonatomic) ColorViewController *colorController;
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) ColorGame *colorGame;
@property (strong, nonatomic) NumberGame *numberGame;
@property (strong, nonatomic) BigLetterGame *alphaGame;
@property (strong, nonatomic) SmallLetterGame *smallGame;
@property (strong, nonatomic) ObjectGame *objectGame;
@property (strong, nonatomic) ShapeGame *shapeGame;
@end

@implementation ViewController
{
    NSArray *_buttons;
}

@synthesize alphaButton;
@synthesize colorButton;
@synthesize audioHelper;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self initGames];
    _buttons = @[self.colorButton, self.shapeButton, self.alphaButton,
                 self.smallButton, self.numberButton, self.objectButton];
    
    for(UIButton *btn in _buttons)
    {
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    audioHelper = [[AudioHelper alloc] init];
    [audioHelper playAppLoadSound];
    
    self.colorController = [self.storyboard instantiateViewControllerWithIdentifier:@"color"];
    self.achievementController = [self.storyboard instantiateViewControllerWithIdentifier:@"achievement"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}

-(void)initGames
{
    self.colorGame = [[ColorGame alloc] init];
    self.numberGame = [[NumberGame alloc] init];
    self.alphaGame = [[BigLetterGame alloc] init];
    self.smallGame = [[SmallLetterGame alloc] init];
    self.shapeGame = [[ShapeGame alloc] init];
    self.objectGame = [[ObjectGame alloc] init];
}

-(void)close
{
    [self dismissViewControllerAnimated:true completion:nil];
}

const int xmargin = 40;
const int ymargin = 40;
const int rows = 3;
const int cols = 2;
const int width = 320;
const int height = 280;
const int xspacing = 48;
const int yspacing = 52;

-(void)showLandscape
{
    for(int i=0; i < cols; i++)
    {
        for(int j=0; j < rows; j++)
        {
            CGFloat pWidth = height;
            CGFloat pHeight = width;
            CGFloat pX = ymargin + j*(pWidth + yspacing);
            CGFloat pY = xmargin + i*(pHeight + xspacing);
            int index = i*rows + j;
            UIButton *btn = (UIButton *)_buttons[index];
            btn.frame = CGRectMake(pX, pY, pWidth, pHeight);
        }
    }
}

-(void)showPortrait
{
    for(int i=0; i < rows; i++)
    {
        for(int j=0; j < cols; j++)
        {
            CGFloat pWidth = width;
            CGFloat pHeight = height;
            CGFloat pX = xmargin + j*(pWidth + xspacing);
            CGFloat pY = ymargin + i*(pHeight + yspacing);
            int index = i*cols + j;
            UIButton *btn = (UIButton *)_buttons[index];
            btn.frame = CGRectMake(pX, pY, pWidth, pHeight);
        }
    }
}

-(NSArray *)getGameRank
{
    NSArray *gameArray = @[self.colorGame, self.shapeGame, self.alphaGame,
                          self.smallGame, self.numberGame, self.objectGame];
    NSMutableArray *rankArray = [[NSMutableArray alloc] init];
    for(Game *game in gameArray)
    {
        GameRank *gameRank = [[GameRank alloc] initWithName:game.name
                                                  andPoints:game.points
                                                    andRank:game.rank];
        [rankArray addObject:gameRank];
    }
    return rankArray;
}

- (IBAction)clickColor:(id)sender {
    if(self.colorGame.over)
    {
        [self.colorGame reset];
    }
    self.colorController.game = self.colorGame;
    [self presentViewController:self.colorController animated:YES completion:nil];
}


- (IBAction)clickNumber:(id)sender {
    if(self.numberGame.over)
    {
        [self.numberGame reset];
    }
    self.colorController.game = self.numberGame;
    [self presentViewController:self.colorController animated:YES completion:nil];
}

- (IBAction)clickAlpha:(id)sender {
    if(self.alphaGame.over)
    {
        [self.alphaGame reset];
    }
    self.colorController.game = self.alphaGame;
    [self presentViewController:self.colorController animated:YES completion:nil];
}

- (IBAction)clickSmall:(id)sender {
    if(self.smallGame.over)
    {
        [self.smallGame reset];
    }
    self.colorController.game = self.smallGame;
    [self presentViewController:self.colorController animated:YES completion:nil];
}

- (IBAction)clickShape:(id)sender {
    if(self.shapeGame.over)
    {
        [self.shapeGame reset];
    }
    self.colorController.game = self.shapeGame;
    [self presentViewController:self.colorController animated:YES completion:nil];
}

- (IBAction)clickObject:(id)sender {
    if(self.objectGame.over)
    {
        [self.objectGame reset];
    }
    self.colorController.game = self.objectGame;
    [self presentViewController:self.colorController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
