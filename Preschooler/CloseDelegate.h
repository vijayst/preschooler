//
//  CloseDelegate.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 25/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CloseDelegate <NSObject>
-(void)close;
@end
