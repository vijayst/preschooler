//
//  ColorViewController.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 26/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "AudioHelper.h"
#import "TTSHelper.h"
#import "AnimationHelper.h"

@interface ColorViewController : UIViewController<AudioPlayedDelegate, SpeechCompletedDelegate, AnimationCompletedDelegate>
{
    int done;
}
@property (strong, nonatomic) IBOutlet UIButton *redButton;
@property (strong, nonatomic) IBOutlet UIButton *greenButton;
@property (strong, nonatomic) IBOutlet UIButton *blueButton;
@property (strong, nonatomic) IBOutlet UIButton *yellowButton;
@property (strong, nonatomic) IBOutlet UIButton *blackButton;
@property (strong, nonatomic) IBOutlet UIButton *whiteButton;
- (IBAction)tap:(UIButton *)sender;
@property (assign, nonatomic) id delegate;
- (IBAction)close:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@end
