//
//  AudioHelper.m
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "AudioHelper.h"

@interface AudioHelper()
-(void)play:(NSURL *)url;
@end

@implementation AudioHelper
@synthesize player;
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"appload" ofType:@"wav" inDirectory:@""];
        apploadUrl = [[NSURL alloc] initFileURLWithPath: path];
        path = [[NSBundle mainBundle] pathForResource:@"gameload" ofType:@"mp3" inDirectory:@""];
        gameloadUrl = [[NSURL alloc] initFileURLWithPath: path];
        path = [[NSBundle mainBundle] pathForResource:@"match" ofType:@"mp3" inDirectory:@""];
        matchUrl = [[NSURL alloc] initFileURLWithPath: path];
        path = [[NSBundle mainBundle] pathForResource:@"nomatch" ofType:@"mp3" inDirectory:@""];
        nomatchUrl = [[NSURL alloc] initFileURLWithPath: path];
    }
    return self;
}

+(void)createAudioSession
{
    NSError *error;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    BOOL success = [[AVAudioSession sharedInstance] setActive:YES error: &error];
    if(!success)
        NSLog(@"The audio session activation error is %@" , error.description);
    else
        NSLog(@"The audio session is activated");
}

-(void)playAppLoadSound
{
    [self play:apploadUrl];
}

-(void)playGameLoadSound
{
    [self play:gameloadUrl];
}

-(void)playMatchSound
{
    [self play:matchUrl];
}

-(void)playNoMatchSound
{
    [self play:nomatchUrl];
}


-(void)play:(NSURL *)url
{
    NSError *error;
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: &error];
    player.delegate = self;
    if(error)
        NSLog(@"Error in loading - %@", error.description);
    [player play];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)pplayer successfully:(BOOL)flag
{
    if(flag) {
        NSString *description = pplayer.url.description;
        
        if([description rangeOfString:@"appload.wav"].location!=NSNotFound)
            if([delegate respondsToSelector:@selector(apploadPlayed)])
                [delegate apploadPlayed];
        
        if([description rangeOfString:@"gameload.mp3"].location!=NSNotFound)
            if([delegate respondsToSelector:@selector(gameloadPlayed)])
                [delegate gameloadPlayed];

        if([description rangeOfString:@"match.mp3"].location!=NSNotFound)
            if([delegate respondsToSelector:@selector(matchPlayed)])
                [delegate matchPlayed];

        
        if([description rangeOfString:@"nomatch.mp3"].location!=NSNotFound)
            if([delegate respondsToSelector:@selector(nomatchPlayed)])
                [delegate nomatchPlayed];

    }
}

@end
