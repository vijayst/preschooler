//
//  ColorViewController.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 26/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ColorViewController.h"
#import "CloseDelegate.h"

@interface ColorViewController ()
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) AnimationHelper *animHelper;
@property (strong, nonatomic) TTSHelper *ttsHelper;
-(void)newGame;
-(void)drawHomeImage;
-(void)showLandscape;
-(void)showPortrait;
@end

@implementation ColorViewController

@synthesize redButton;
@synthesize greenButton;
@synthesize blackButton;
@synthesize blueButton;
@synthesize yellowButton;
@synthesize whiteButton;
@synthesize homeButton;

@synthesize animHelper;
@synthesize audioHelper;
@synthesize ttsHelper;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    audioHelper = [[AudioHelper alloc] init];
    audioHelper.delegate = self;
    animHelper = [[AnimationHelper alloc] init];
    animHelper.delegate = self;
    ttsHelper = [[TTSHelper alloc] init];
    ttsHelper.delegate = self;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [homeButton setTitle:@"" forState:UIControlStateNormal];
    
    [redButton setTitle:@"" forState:UIControlStateNormal];
    redButton.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
    [greenButton setTitle:@"" forState:UIControlStateNormal];
    greenButton.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
    [blueButton setTitle:@"" forState:UIControlStateNormal];
    blueButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.5];
    [yellowButton setTitle:@"" forState:UIControlStateNormal];
    yellowButton.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0 alpha:0.8];
    [blackButton setTitle:@"" forState:UIControlStateNormal];
    blackButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [whiteButton setTitle:@"" forState:UIControlStateNormal];
    whiteButton.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    
    redButton.tag = 0;
    greenButton.tag = 1;
    blueButton.tag = 2;
    yellowButton.tag = 3;
    blackButton.tag = 4;
    whiteButton.tag = 5;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self newGame];
    [super viewDidAppear:animated];
}

-(void)newGame
{
    done = 0;
    redButton.enabled = false;
    greenButton.enabled = false;
    blueButton.enabled = false;
    yellowButton.enabled = false;
    whiteButton.enabled = false;
    blackButton.enabled = false;
    
    [audioHelper playGameLoadSound];
    [animHelper zoomIn:redButton];
    [animHelper zoomIn:greenButton];
    [animHelper zoomIn:blueButton];
    [animHelper zoomIn:yellowButton];
    [animHelper zoomIn:blackButton];
    [animHelper zoomIn:whiteButton];
}

-(void)gameloadPlayed
{
    redButton.enabled = true;
    greenButton.enabled = true;
    blueButton.enabled = true;
    yellowButton.enabled = true;
    whiteButton.enabled = true;
    blackButton.enabled = true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tap:(UIButton *)sender {
    self.view.userInteractionEnabled = false;
    [audioHelper playMatchSound];
    sender.enabled = false;
    
    switch(sender.tag)
    {
        case 0:
            [animHelper zoom:sender state:@"red"];
            break;
        case 1:
            [animHelper zoom:sender state:@"green"];
            break;
        case 2:
            [animHelper zoom:sender state:@"blue"];
            break;
        case 3:
            [animHelper zoom:sender state:@"yellow"];
            break;
        case 4:
            [animHelper zoom:sender state:@"black"];
            break;
        case 5:
            [animHelper zoom:sender state:@"white"];
            break;
    }
}

-(void)animationCompleted:(NSString *)state
{
    [ttsHelper speakAnswer:[NSString stringWithFormat:@"%@ color.", state]];
    self.view.userInteractionEnabled = true;
}

-(void)answerCompleted
{
    done++;
    if(done>=6)
    {
        [self newGame];
    }
}

- (IBAction)close:(id)sender {
    if([delegate respondsToSelector:@selector(close)]) {
        [delegate close];
    }
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}

static int pageMargin = 50;
static int xMargin = 30;
static int yMargin = 30;

-(void)showPortrait
{
    CGSize size = [self.view bounds].size;
    float homeWidth = 80;
    float homeHeight = 80;
    [homeButton setFrame:CGRectMake(pageMargin, pageMargin, homeWidth, homeHeight)];
    [self drawHomeImage];
    
    float cellWidth = (size.width - 2*pageMargin-xMargin)/2;
    float cellHeight = (size.height - homeHeight - 2*pageMargin - 3*yMargin)/3;
    
    [redButton setFrame:CGRectMake(pageMargin, pageMargin+homeHeight+yMargin, cellWidth, cellHeight)];
    [greenButton setFrame:CGRectMake(pageMargin + cellWidth + xMargin, pageMargin+homeHeight+yMargin, cellWidth, cellHeight)];
    [blueButton setFrame:CGRectMake(pageMargin, pageMargin+homeHeight+yMargin+cellHeight+yMargin, cellWidth, cellHeight)];
    [yellowButton setFrame:CGRectMake(pageMargin + cellWidth + xMargin, pageMargin+homeHeight+yMargin+cellHeight+yMargin, cellWidth, cellHeight)];
    [blackButton setFrame:CGRectMake(pageMargin, pageMargin+homeHeight+yMargin+2*(cellHeight+yMargin), cellWidth, cellHeight)];
    [whiteButton setFrame:CGRectMake(pageMargin + cellWidth + xMargin, pageMargin+homeHeight+yMargin+2*(cellHeight+yMargin), cellWidth, cellHeight)];
    
}

-(void)showLandscape
{
    CGSize size = [self.view bounds].size;
    float homeWidth = 80;
    float homeHeight = 80;
    [homeButton setFrame:CGRectMake(pageMargin, pageMargin, homeWidth, homeHeight)];
    [self drawHomeImage];
    
    float cellWidth = (size.width - 2*pageMargin-2*xMargin)/3;
    float cellHeight = (size.height  - homeHeight - 2*pageMargin - 2*yMargin)/2;
    
    [redButton setFrame:CGRectMake(pageMargin, pageMargin+homeHeight+yMargin, cellWidth, cellHeight)];
    [greenButton setFrame:CGRectMake(pageMargin + cellWidth + xMargin, pageMargin+homeHeight+yMargin, cellWidth, cellHeight)];
    [blueButton setFrame:CGRectMake(pageMargin + 2*(cellWidth + xMargin), pageMargin+homeHeight+yMargin, cellWidth, cellHeight)];
    [yellowButton setFrame:CGRectMake(pageMargin, pageMargin+homeHeight+yMargin+cellHeight+yMargin, cellWidth, cellHeight)];
    [blackButton setFrame:CGRectMake(pageMargin + cellWidth + xMargin, pageMargin+homeHeight+yMargin+cellHeight+yMargin, cellWidth, cellHeight)];
    [whiteButton setFrame:CGRectMake(pageMargin + 2*(cellWidth + xMargin),pageMargin+homeHeight+yMargin+cellHeight+yMargin, cellWidth, cellHeight)];
}

-(void)drawHomeImage
{
    UIImage *homeImage = [UIImage imageNamed:@"home.png"];
    UIGraphicsBeginImageContextWithOptions(homeButton.frame.size, NO, 0.f);
    [homeImage drawInRect:CGRectMake(0.f, 0.f, homeButton.frame.size.width, homeButton.frame.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    homeButton.backgroundColor = [UIColor colorWithPatternImage:newImage];
}
@end
