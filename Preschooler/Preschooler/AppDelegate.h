//
//  AppDelegate.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 25/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
