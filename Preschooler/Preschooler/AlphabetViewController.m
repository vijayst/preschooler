//
//  AlphabetViewController.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 25/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "AlphabetViewController.h"
#import "CloseDelegate.h"


@interface AlphabetViewController ()
-(void)drawHomeImage;
-(void)showLandscape;
-(void)showPortrait;
-(void)initView;
-(void)getQuiz;
-(void)setQuestion;
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) AnimationHelper *animHelper;
@property (strong, nonatomic) TTSHelper *ttsHelper;
@end

@implementation AlphabetViewController
@synthesize alphabet;
@synthesize choice1;
@synthesize choice2;
@synthesize choice3;
@synthesize choice4;
@synthesize segmentedControl1;
@synthesize segmentedControl2;
@synthesize segmentedControl3;
@synthesize delegate;
@synthesize home;

@synthesize audioHelper;
@synthesize animHelper;
@synthesize ttsHelper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initArray];
    audioHelper = [[AudioHelper alloc] init];
    audioHelper.delegate = self;
    animHelper = [[AnimationHelper alloc] init];
    animHelper.delegate = self;
    ttsHelper = [[TTSHelper alloc] init];
    ttsHelper.delegate = self;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    alphabet.text = @"";
    alphabet.backgroundColor = [UIColor colorWithRed:0.8 green:0.4 blue:1 alpha:1];
    alphabet.font = [UIFont fontWithName:@"Arial" size:120];
    alphabet.textColor = [UIColor whiteColor];
    
    alphabet.textAlignment = NSTextAlignmentCenter;
    choice1.backgroundColor = [UIColor colorWithRed:0.7 green:0.3 blue:0.3 alpha:1];
    choice2.backgroundColor = [UIColor colorWithRed:0.5 green:0.8 blue:0.5 alpha:1];
    choice3.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.5 alpha:1];
    choice4.backgroundColor = [UIColor colorWithRed:0.4 green:0.6 blue:1 alpha:1];
    
    
    [choice1 setTitle:@"" forState:UIControlStateNormal];
    [choice2 setTitle:@"" forState:UIControlStateNormal];
    [choice3 setTitle:@"" forState:UIControlStateNormal];
    [choice4 setTitle:@"" forState:UIControlStateNormal];

    choice1.tag = 0;
    choice2.tag = 1;
    choice3.tag = 2;
    choice4.tag = 3;

    segmentedControl1.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    segmentedControl2.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    segmentedControl3.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    
    segmentedControl1.tag = 0;
    segmentedControl2.tag = 1;
    segmentedControl3.tag = 2;
    
    [segmentedControl1 removeAllSegments];
    [segmentedControl1 initWithItems:[alphabetArray subarrayWithRange:NSMakeRange(0, 8)]];
    [segmentedControl2 removeAllSegments];
    [segmentedControl2 initWithItems:[alphabetArray subarrayWithRange:NSMakeRange(8, 8)]];
    [segmentedControl3 removeAllSegments];
    [segmentedControl3 initWithItems:[alphabetArray subarrayWithRange:NSMakeRange(16, 10)]];
    
    question = 0;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self initView];
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initArray
{
    alphabetArray = [NSArray arrayWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    textArray = [NSArray arrayWithObjects:@"Apple", @"Ball", @"Cat", @"Dog", @"Elephant", @"Fish", @"Giraffe", @"House", @"IceCream", @"Jelly", @"Kite", @"Lion", @"Monkey", @"Nail", @"Octupus", @"Pencil", @"Question", @"Rose", @"Sun", @"Tomato", @"Umbrella", @"Van", @"Water", @"Xylophone", @"Yarn", @"Zebra", nil];
}

-(void)initView
{
    [audioHelper playGameLoadSound];
    [self getQuiz];
    
    [alphabet setText:[alphabetArray objectAtIndex:question]];
    
    [choice1 setImage:[self getImage:choices[0]] forState:UIControlStateNormal];
    [choice2 setImage:[self getImage:choices[1]] forState:UIControlStateNormal];
    [choice3 setImage:[self getImage:choices[2]] forState:UIControlStateNormal];
    [choice4 setImage:[self getImage:choices[3]] forState:UIControlStateNormal];
    
    choice1.enabled = false;
    choice2.enabled = false;
    choice3.enabled = false;
    choice4.enabled = false;
    
    [self setQuestion];
    
    [animHelper zoomIn:alphabet];
    [animHelper fadeIn:choice1];
    [animHelper fadeIn:choice2];
    [animHelper fadeIn:choice3];
    [animHelper fadeIn:choice4];
}

-(void)gameloadPlayed
{
    [ttsHelper speakPrompt:[[alphabetArray objectAtIndex:question] lowercaseString]];
}

-(void)promptCompleted
{
    choice1.enabled = TRUE;
    choice2.enabled = TRUE;
    choice3.enabled = TRUE;
    choice4.enabled = TRUE;
}

-(void)animationCompleted:(NSString *)state
{
    if([state isEqualToString:@"match"]) {
        [ttsHelper speakAnswer:[NSString stringWithFormat:@"%@ for %@", [[alphabetArray objectAtIndex:question] lowercaseString], [textArray objectAtIndex:question]]];
    }
}

-(void)answerCompleted
{
    question++;
    if(question>25)
        question = 0;
    [self initView];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if(flag) {
        NSLog(@"%@", player.data.description);
    }
}

-(void)getQuiz
{
    choices[0] = choices[1] = choices[2] = choices[3] = -1;
    
    for(int idx=0; idx<3; idx++)
    {
        bool found;
        int newIndex;
        do {
            found = false;
            newIndex = arc4random()%26;
            for(int idx2=0; idx2<idx; idx2++)
            {
                if(newIndex==choices[idx2] || newIndex==question)
                    found = true;
            }
        } while (found);
        choices[idx] = newIndex;
    }
    
    answer = arc4random() % 4;
    if(answer==4)
        choices[3] = question;
    else {
        choices[3] = choices[answer];
        choices[answer] = question;
    }
}

-(void)setQuestion
{
    segmentedControl3.selectedSegmentIndex = segmentedControl2.selectedSegmentIndex = segmentedControl1.selectedSegmentIndex = UISegmentedControlNoSegment;
    if(question<8)
        segmentedControl1.selectedSegmentIndex = question;
    else if(question<16)
        segmentedControl2.selectedSegmentIndex = question-8;
    else
        segmentedControl3.selectedSegmentIndex = question-16;
}

-(UIImage *)getImage:(int)choice
{
    NSString *imagePath = [NSString stringWithFormat:@"%@-%@.png", [alphabetArray objectAtIndex:choice] , [textArray objectAtIndex:choice]];
    return [UIImage imageNamed:imagePath];
}

-(IBAction)tapImage:(UIButton *)sender
{
    if(sender.tag == answer){
        [audioHelper playMatchSound];
        [animHelper zoom:sender state:@"match"];
    } else {
        sender.enabled = false;
        [audioHelper playNoMatchSound];
        [animHelper zoom:sender state:@"nomatch"];
    }
}

-(IBAction)selectAlphabet:(UISegmentedControl *)sender
{
    question = sender.tag*8 + sender.selectedSegmentIndex;
    [self initView];
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}

static int pageMargin = 40;
static int xMargin = 20;
static int yMargin = 20;

-(void)showPortrait
{
    CGRect bounds = [self.view bounds];
    float controlHeight = (bounds.size.height - 2*pageMargin - 3*yMargin)/4;
    [alphabet setFrame:CGRectMake(pageMargin, pageMargin, bounds.size.width-2*pageMargin, controlHeight)];
    float controlWidth = (bounds.size.width - 2*pageMargin-yMargin)/2;
    [choice1 setFrame:CGRectMake(pageMargin, pageMargin+controlHeight+yMargin, controlWidth, controlHeight)];
    [choice2 setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin+controlHeight+yMargin, controlWidth, controlHeight)];
    [choice3 setFrame:CGRectMake(pageMargin, pageMargin+2*(controlHeight+yMargin), controlWidth, controlHeight)];
    [choice4 setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin+2*(controlHeight+yMargin), controlWidth, controlHeight)];
    float segmentHeight = (controlHeight - 2*yMargin)/3;
    float homeHeight = controlHeight - segmentHeight - yMargin;
    float homeWidth = homeHeight;
    int segmentWidth1 = floor((bounds.size.width - 2*pageMargin- homeWidth-xMargin)/8)*8-1;
    int segmentWidth3 = floor((bounds.size.width - 2*pageMargin)/10)*10-1;
    [home setFrame:CGRectMake(pageMargin, pageMargin+3*(controlHeight + yMargin), homeWidth, homeHeight)];
    [segmentedControl1 setFrame:CGRectMake(pageMargin + homeWidth + xMargin, pageMargin + 3*(controlHeight+yMargin), segmentWidth1, segmentHeight)];
    [segmentedControl2 setFrame:CGRectMake(pageMargin + homeWidth + xMargin, pageMargin + 3*(controlHeight+yMargin) + segmentHeight+yMargin, segmentWidth1, segmentHeight)];
    [segmentedControl3 setFrame:CGRectMake(pageMargin, pageMargin + 3*(controlHeight+yMargin) + 2*(segmentHeight+yMargin), segmentWidth3, segmentHeight)];
    [self drawHomeImage];
}

-(void)showLandscape
{
    CGRect bounds = [self.view bounds];
    float controlHeight = (bounds.size.height - 2*pageMargin - 2*yMargin)/3;
    float controlWidth = (bounds.size.width -2*pageMargin-2*xMargin)/3;
    [alphabet setFrame:CGRectMake(pageMargin, pageMargin, controlWidth, 2*controlHeight+yMargin)];
    [choice1 setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin, controlWidth, controlHeight)];
    [choice2 setFrame:CGRectMake(pageMargin + 2*(controlWidth + xMargin), pageMargin, controlWidth, controlHeight)];
    [choice3 setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin+controlHeight+yMargin, controlWidth, controlHeight)];
    [choice4 setFrame:CGRectMake(pageMargin + 2*(controlWidth + xMargin), pageMargin+controlHeight+yMargin, controlWidth, controlHeight)];
    float segmentHeight = (controlHeight - 2*yMargin)/3;
    float homeHeight = controlHeight;
    float homeWidth = controlHeight;
    float segmentWidth = bounds.size.width - 2*pageMargin - homeWidth - xMargin;
    int segmentWidth1 = floor(segmentWidth/8)*8-1;
    int segmentWidth3 = floor(segmentWidth/10)*10-1;
    [home setFrame:CGRectMake(pageMargin, pageMargin+2*(controlHeight + yMargin), homeWidth, homeHeight)];
    [segmentedControl1 setFrame:CGRectMake(pageMargin + homeWidth + xMargin, pageMargin + 2*(controlHeight+yMargin), segmentWidth1, segmentHeight)];
    [segmentedControl2 setFrame:CGRectMake(pageMargin + homeWidth + xMargin, pageMargin + 2*(controlHeight+yMargin) + segmentHeight+yMargin, segmentWidth1, segmentHeight)];
    [segmentedControl3 setFrame:CGRectMake(pageMargin + homeWidth + xMargin, pageMargin + 2*(controlHeight+yMargin) + 2*(segmentHeight+yMargin), segmentWidth3, segmentHeight)];
    [self drawHomeImage];
}

-(void)drawHomeImage
{
    UIImage *homeImage = [UIImage imageNamed:@"home.png"];
    UIGraphicsBeginImageContextWithOptions(home.frame.size, NO, 0.f);
    [homeImage drawInRect:CGRectMake(0.f, 0.f, home.frame.size.width, home.frame.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    home.backgroundColor = [UIColor colorWithPatternImage:newImage];
}


- (IBAction)back:(id)sender {
    if([delegate respondsToSelector:@selector(close)]) {
        [delegate close];
    }
}
@end
