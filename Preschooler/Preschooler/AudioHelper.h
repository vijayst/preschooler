//
//  AudioHelper.h
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>

@protocol AudioPlayedDelegate<NSObject>
@optional
-(void)apploadPlayed;
-(void)gameloadPlayed;
-(void)matchPlayed;
-(void)nomatchPlayed;
@end

@interface AudioHelper : NSObject<AVAudioPlayerDelegate>
{
    NSURL *apploadUrl;
    NSURL *gameloadUrl;
    NSURL *matchUrl;
    NSURL *nomatchUrl;
}
-(void)playAppLoadSound;
-(void)playGameLoadSound;
-(void)playMatchSound;
-(void)playNoMatchSound;
+(void)createAudioSession;

@property (assign, nonatomic) id delegate;
@property (nonatomic, strong) AVAudioPlayer *player;
@end
