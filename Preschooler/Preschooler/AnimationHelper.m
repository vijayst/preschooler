//
//  AnimationHelper.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 21/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "AnimationHelper.h"

@implementation AnimationHelper

@synthesize delegate;

-(void) zoomIn:(UIView *)view
{
    view.transform = CGAffineTransformMakeScale(0.4, 0.4);
	[UIView animateWithDuration:5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

-(void) fadeIn:(UIView *)view
{
    [view setAlpha:0.1];
	[UIView animateWithDuration:5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [view setAlpha:1];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

-(void) zoom:(UIView *)view state:(NSString *)state
{
	[UIView animateWithDuration:3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         view.transform = CGAffineTransformMakeScale(0.2, 0.2);
                         view.alpha = 1;
                         [UIView animateWithDuration:5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              view.transform = CGAffineTransformIdentity;
                                          }
                                          completion:^(BOOL finished){
                                              if([delegate respondsToSelector:@selector(animationCompleted:)])
                                                  [delegate animationCompleted:state];
                                          }];
                     }];
}

@end
