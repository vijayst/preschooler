//
//  AlphabetViewController.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 25/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "AudioHelper.h"
#import "TTSHelper.h"
#import "AnimationHelper.h"

@interface AlphabetViewController : UIViewController<AudioPlayedDelegate, SpeechCompletedDelegate, AnimationCompletedDelegate>
{
    NSArray* alphabetArray;
    NSArray* textArray;
    
    int question;
    int answer;
    int choices[4];
}

@property (strong, nonatomic) IBOutlet UILabel *alphabet;
@property (strong, nonatomic) IBOutlet UIButton *choice1;
@property (strong, nonatomic) IBOutlet UIButton *choice2;
@property (strong, nonatomic) IBOutlet UIButton *choice3;
@property (strong, nonatomic) IBOutlet UIButton *choice4;
@property (strong, nonatomic) IBOutlet UIButton *home;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl1;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl2;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl3;
@property (assign, nonatomic) id delegate;

-(IBAction)selectAlphabet:(UISegmentedControl *)sender;
-(IBAction)tapImage:(UIButton *)sender;
- (IBAction)back:(id)sender;
@end
