//
//  ShapeGame.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ShapeGame.h"

@implementation ShapeGame

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.name = @"SHAPES";
        self.gameType = GTShapes;
        [self getData];
    }
    return self;
}

-(void)getQuiz
{
    [super getQuiz];
    NSMutableArray *imageChoices = [[NSMutableArray alloc] init];
    for(NSNumber *choiceNumber in _choiceNumbers)
    {
        int choiceIndex = [choiceNumber intValue];
        ImageChoice *imageChoice = [[ImageChoice alloc] initWithName:_names[choiceIndex]
                                                            andImage:_data[choiceIndex]];
        [imageChoices addObject:imageChoice];
    }
    self.quiz.choices = imageChoices;
}

-(void)getData
{
    _data = @[[UIImage imageNamed:@"circle.png"],
              [UIImage imageNamed:@"crescent.png"],
              [UIImage imageNamed:@"square.png"],
              [UIImage imageNamed:@"rectangle.png"],
              [UIImage imageNamed:@"triangle.png"],
              [UIImage imageNamed:@"oval.png"]];
    
    _names = @[@"circle", @"crescent", @"square",
               @"rectangle", @"triangle", @"oval"];
    
    self.total = [_data count];
    
    _indices = [[NSMutableArray alloc] init];
    for(int i=0; i < self.total; i++)
    {
        [_indices addObject:[NSNumber numberWithInt:i]];
    }
}


@end
