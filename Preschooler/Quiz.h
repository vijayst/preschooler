//
//  Quiz.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Quiz : NSObject
@property (strong, nonatomic) NSString *prompt;
@property (strong, nonatomic) NSArray *choices;
@property BOOL answered;
@property int attempts;
@property int answerPosition;
@end

@interface ColorChoice : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIColor *color;
-(id)initWithName:(NSString *)name andColor:(UIColor *)color;
@end

@interface WordChoice : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *word;
-(id)initWithName:(NSString *)name andWord:(NSString *)word;
@end

@interface ImageChoice : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *image;
-(id)initWithName:(NSString *)name andImage:(UIImage *)image;
@end