//
//  AnimationHelper.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 21/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AnimationCompletedDelegate
@optional
-(void)animationCompleted:(NSString *)state;
@end

@interface AnimationHelper : NSObject
-(void) zoomIn:(UIView *)view;
-(void) fadeIn:(UIView *)view;
-(void) zoom:(UIView *)view state:(NSString *)state;
-(void) shimmer:(UIView *)view;
@property (assign, nonatomic) id delegate;
@end
