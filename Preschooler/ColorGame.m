//
//  ColorGame.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ColorGame.h"

@implementation ColorChoice

-(id)initWithName:(NSString *)name andColor:(UIColor *)color
{
    self = [super init];
    if(self!=nil)
    {
        self.name = name;
        self.color = color;
    }
    return self;
}

@end

@implementation ColorGame
{

}

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.name = @"COLORS";
        self.gameType = GTColors;
        [self getData];
    }
    return self;
}

-(void)getQuiz
{
    [super getQuiz];
    NSMutableArray *colorChoices = [[NSMutableArray alloc] init];
    for(NSNumber *choiceNumber in _choiceNumbers)
    {
        int choiceIndex = [choiceNumber intValue];
        ColorChoice *colorChoice = [[ColorChoice alloc] initWithName:_names[choiceIndex]
                                                            andColor:_data[choiceIndex]];
        [colorChoices addObject:colorChoice];
    }
    self.quiz.choices = colorChoices;
}

-(void)getData
{
    _data = @[[UIColor redColor],
              [UIColor greenColor],
              [UIColor blueColor],
              [UIColor blackColor],
              [UIColor whiteColor],
              [UIColor purpleColor],
              [UIColor yellowColor],
              [UIColor orangeColor],
              [UIColor brownColor]];
    
    _names = @[@"red", @"green", @"blue", @"black",
               @"white", @"purple", @"yellow", @"orange",
               @"brown"];
    
    self.total = [_data count];
    
    _indices = [[NSMutableArray alloc] init];
    for(int i=0; i < self.total; i++)
    {
        [_indices addObject:[NSNumber numberWithInt:i]];
    }
}

@end
