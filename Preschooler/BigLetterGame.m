//
//  BigLetterGame.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "BigLetterGame.h"

@implementation BigLetterGame

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.name = @"BIG LETTERS";
        self.gameType = GTBigLetters;
        [self getData];
    }
    return self;
}

-(void)getQuiz
{
    [super getQuiz];
    NSMutableArray *wordChoices = [[NSMutableArray alloc] init];
    for(NSNumber *choiceNumber in _choiceNumbers)
    {
        int choiceIndex = [choiceNumber intValue];
        WordChoice *wordChoice = [[WordChoice alloc] initWithName:_names[choiceIndex]
                                                          andWord:_data[choiceIndex]];
        [wordChoices addObject:wordChoice];
    }
    self.quiz.choices = wordChoices;
}

-(void)getData
{
    _data = @[@"A", @"B", @"C", @"D", @"E",
              @"F", @"G", @"H", @"I", @"J",
              @"K", @"L", @"M", @"N", @"O",
              @"P", @"Q", @"R", @"S", @"T",
              @"U", @"V", @"W", @"X", @"Y",
              @"Z"];
    
    _names = [_data copy];
    
    self.total = [_data count];
    
    _indices = [[NSMutableArray alloc] init];
    for(int i=0; i < self.total; i++)
    {
        [_indices addObject:[NSNumber numberWithInt:i]];
    }
}

@end
