//
//  StarControl.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "StarControl.h"

@implementation StarControl
{
    GameRank *_rank;
}

-(id)initWithRank:(GameRank *)rank
{
    self = [super init];
    if(self!=nil)
    {
        _rank = rank;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,350,110)];
        self.titleLabel.text = _rank.name;
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont systemFontOfSize:40.0F];
        [self addSubview:self.titleLabel];
        self.pointsLabel = [[UILabel alloc] initWithFrame:CGRectMake(360,0,110,110)];
        self.pointsLabel.layer.cornerRadius = 10.0F;
        self.pointsLabel.numberOfLines = 2;
        self.pointsLabel.backgroundColor = [UIColor redColor];
        self.pointsLabel.textColor = [UIColor whiteColor];
        self.pointsLabel.textAlignment = NSTextAlignmentCenter;
        NSString *pointsText = [NSString stringWithFormat:@"%d\nPOINTS", _rank.points];
        NSUInteger loc = [pointsText rangeOfString:@"\n"].location;
        NSMutableAttributedString *pointsAttrText = [[NSMutableAttributedString alloc] initWithString:pointsText];
        [pointsAttrText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:50.0F]
                           range:NSMakeRange(0,loc)];
        [pointsAttrText addAttribute:NSFontAttributeName
                               value:[UIFont boldSystemFontOfSize:20.0F]
                               range:NSMakeRange(loc+1,6)];
        self.pointsLabel.attributedText = pointsAttrText;
        [self addSubview:self.pointsLabel];
    }
    return self;
}

-(void)layoutSubviews
{
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
