//
//  ProgressBar.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 02/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressBar : UIView
{
    float value;
}
@property (strong, nonatomic) UIColor *fillColor;
@property (strong, nonatomic) UIColor *backColor;
-(void)setProgress:(float)pValue;
@end
