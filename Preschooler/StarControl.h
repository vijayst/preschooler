//
//  StarControl.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameRank.h"

@interface StarControl : UIControl
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *pointsLabel;
@property (strong, nonatomic) UIImageView *starImage;
-(id)initWithRank:(GameRank *)rank;
@end
