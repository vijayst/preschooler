//
//  ColorViewController.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 26/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "AudioHelper.h"
#import "TTSHelper.h"
#import "AnimationHelper.h"
#import "Game.h"

@interface ColorViewController : UIViewController<AudioPlayedDelegate, SpeechCompletedDelegate, AnimationCompletedDelegate>
{
    NSTimer *timer;
}
@property (strong, nonatomic) Game* game;
@property (strong, nonatomic) IBOutlet UIButton *choice1Button;
@property (strong, nonatomic) IBOutlet UIButton *choice2Button;
@property (strong, nonatomic) IBOutlet UIButton *choice3Button;
@property (strong, nonatomic) IBOutlet UIButton *choice4Button;
@property (strong, nonatomic) IBOutlet UIButton *choice5Button;
@property (strong, nonatomic) IBOutlet UIButton *choice6Button;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *gameButton;
@property (strong, nonatomic) IBOutlet UIButton *stopButton;
@property (strong,nonatomic) UIProgressView *progressBar;
@property (strong, nonatomic) IBOutlet UILabel *captionLabel;
- (IBAction)tap:(UIButton *)sender;
- (IBAction)close:(id)sender;
- (IBAction)newGame:(id)sender;
- (IBAction)stopGame:(id)sender;
@end
