//
//  SmallLetterGame.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "SmallLetterGame.h"

@implementation SmallLetterGame

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.name = @"SMALL LETTERS";
        self.gameType = GTSmallLetters;
        [self getData];
    }
    return self;
}

-(void)getQuiz
{
    [super getQuiz];
    NSMutableArray *wordChoices = [[NSMutableArray alloc] init];
    for(NSNumber *choiceNumber in _choiceNumbers)
    {
        int choiceIndex = [choiceNumber intValue];
        WordChoice *wordChoice = [[WordChoice alloc] initWithName:_names[choiceIndex]
                                                          andWord:_data[choiceIndex]];
        [wordChoices addObject:wordChoice];
    }
    self.quiz.choices = wordChoices;
}

-(void)getData
{
    _data = @[@"a", @"b", @"c", @"d", @"e",
              @"f", @"g", @"h", @"i", @"j",
              @"k", @"l", @"m", @"n", @"o",
              @"p", @"q", @"r", @"s", @"t",
              @"u", @"v", @"w", @"x", @"y",
              @"z"];
    
    _names = [_data copy];
    
    self.total = [_data count];
    
    _indices = [[NSMutableArray alloc] init];
    for(int i=0; i < self.total; i++)
    {
        [_indices addObject:[NSNumber numberWithInt:i]];
    }
}

@end
