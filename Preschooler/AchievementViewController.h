//
//  AchievementControllerViewController.h
//  Preschooler
//
//  Created by Vijay Thirugnanam on 27/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchievementViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
- (IBAction)clickHome:(id)sender;
@end
