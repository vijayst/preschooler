//
//  NumberGame.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "NumberGame.h"

@implementation NumberGame

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.name = @"NUMBERS";
        self.gameType = GTNumbers;
        [self getData];
    }
    return self;
}

-(void)getQuiz
{
    [super getQuiz];
    NSMutableArray *wordChoices = [[NSMutableArray alloc] init];
    for(NSNumber *choiceNumber in _choiceNumbers)
    {
        int choiceIndex = [choiceNumber intValue];
        WordChoice *wordChoice = [[WordChoice alloc] initWithName:_names[choiceIndex]
                                                            andWord:_data[choiceIndex]];
        [wordChoices addObject:wordChoice];
    }
    self.quiz.choices = wordChoices;
}

-(void)getData
{
    _data = @[@"1",
              @"2",
              @"3",
              @"4",
              @"5",
              @"6",
              @"7",
              @"8",
              @"9",
              @"10"];
    
    _names = @[@"one", @"two", @"three", @"four", @"five",
               @"six", @"seven", @"eight", @"nine", @"ten"];
    
    self.total = [_data count];
    
    _indices = [[NSMutableArray alloc] init];
    for(int i=0; i < self.total; i++)
    {
        [_indices addObject:[NSNumber numberWithInt:i]];
    }
}

@end
