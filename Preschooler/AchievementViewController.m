//
//  AchievementControllerViewController.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 27/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "AchievementViewController.h"
#import "ViewController.h"
#import "GameRank.h"
#import "StarControl.h"

@interface AchievementViewController ()
@property (strong, nonatomic) NSArray *rankArray;
@end

@implementation AchievementViewController
{
    NSMutableArray *controlArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    ViewController *controller = (ViewController *)self.presentingViewController;
    self.rankArray = [controller getGameRank];
    controlArray = [[NSMutableArray alloc] init];
    for(GameRank *rank in self.rankArray)
    {
        StarControl *control = [[StarControl alloc] initWithRank:rank];
        [self.view addSubview:control];
        [controlArray addObject:control];
    }
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}

-(void)showLandscape
{
    int xmargin = 22;
    int ymargin = 20;
    int yspacing = 40;
    int width = 980;
    int row1 = 70;
    int row2 = 64;
    int cwidth = 450;
    int cheight = 110;
    int cxspacing = 40;
    int cyspacing = 20;
    
    CGRect titleFrame = CGRectMake(xmargin, ymargin, width, row1);
    self.titleLabel.frame = titleFrame;
    
    int xpos = xmargin;
    int ypos = ymargin + row1 + yspacing;
    int index = 0;
    for(UIControl *control in controlArray)
    {
        xpos = (index % 2 == 0) ? xmargin : xmargin + cwidth + cxspacing;
        CGRect controlFrame = CGRectMake(xpos, ypos, width, cheight);
        control.frame = controlFrame;
        index++;
        if(index % 2 == 0)
            ypos += cheight + cyspacing;
    }
    
    ypos += yspacing - cyspacing;
    CGRect homeFrame = CGRectMake(xmargin + width/2 - row2/2,
                                  ypos,row2, row2);
    self.homeButton.frame = homeFrame;
}

-(void)showPortrait
{
    int xmargin = 30;
    int ymargin = 30;
    int yspacing = 35;
    int width = 708;
    int row1 = 70;
    int row2 = 64;
    int cheight = 110;
    int cspacing = 20;
    
    CGRect titleFrame = CGRectMake(xmargin, ymargin, width, row1);
    self.titleLabel.frame = titleFrame;
    
    int ypos = ymargin + row1 + yspacing;
    for(UIControl *control in controlArray)
    {
        CGRect controlFrame = CGRectMake(xmargin, ypos, width, cheight);
        control.frame = controlFrame;
        ypos += cheight + cspacing;
    }
    
    ypos += yspacing - cspacing;
    CGRect homeFrame = CGRectMake(xmargin + width/2 - row2/2,
                                  ypos,row2, row2);
    self.homeButton.frame = homeFrame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickHome:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES
                                                      completion:nil];
}
@end
