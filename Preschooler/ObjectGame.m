//
//  ObjectGame.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ObjectGame.h"

@implementation ObjectGame

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.name = @"OBJECTS";
        self.gameType = GTObjects;
        [self getData];
    }
    return self;
}

-(void)getQuiz
{
    [super getQuiz];
    NSMutableArray *imageChoices = [[NSMutableArray alloc] init];
    for(NSNumber *choiceNumber in _choiceNumbers)
    {
        int choiceIndex = [choiceNumber intValue];
        ImageChoice *imageChoice = [[ImageChoice alloc] initWithName:_names[choiceIndex]
                                                          andImage:_data[choiceIndex]];
        [imageChoices addObject:imageChoice];
    }
    self.quiz.choices = imageChoices;
}

-(void)getData
{
    _data = @[[UIImage imageNamed:@"A-Apple.png"],
              [UIImage imageNamed:@"B-Ball.png"],
              [UIImage imageNamed:@"C-Cat.png"],
              [UIImage imageNamed:@"D-Dog.png"],
              [UIImage imageNamed:@"E-Elephant.png"],
              [UIImage imageNamed:@"F-Fish.png"],
              [UIImage imageNamed:@"G-Giraffe.png"],
              [UIImage imageNamed:@"H-House.png"],
              [UIImage imageNamed:@"I-IceCream.png"],
              [UIImage imageNamed:@"J-Jelly.png"],
              [UIImage imageNamed:@"K-Kite.png"],
              [UIImage imageNamed:@"L-Lion.png"],
              [UIImage imageNamed:@"M-Monkey.png"],
              [UIImage imageNamed:@"N-Nail.png"],
              [UIImage imageNamed:@"O-Octupus.png"],
              [UIImage imageNamed:@"P-Pencil.png"],
              [UIImage imageNamed:@"Q-Question.png"],
              [UIImage imageNamed:@"R-Rose.png"],
              [UIImage imageNamed:@"S-Sun.png"],
              [UIImage imageNamed:@"T-Tomato.png"],
              [UIImage imageNamed:@"U-Umbrella.png"],
              [UIImage imageNamed:@"V-Van.png"],
              [UIImage imageNamed:@"W-Water.png"],
              [UIImage imageNamed:@"X-Xylophone.png"],
              [UIImage imageNamed:@"Y-Yarn.png"],
              [UIImage imageNamed:@"Z-Zebra.png"]];
    
    _names = @[@"Apple", @"Ball", @"Cat", @"Dog", @"Elephant",
               @"Fish", @"Giraffe", @"House", @"IceCream", @"Jelly",
               @"Kite", @"Lion", @"Monkey", @"Nail", @"Octupus",
               @"Pencil", @"Question", @"Rose", @"Sun", @"Tomato",
               @"Umbrella", @"Van", @"Water", @"Xylophone", @"Yarn",
               @"Zebra"];
    
    self.total = [_data count];
    
    _indices = [[NSMutableArray alloc] init];
    for(int i=0; i < self.total; i++)
    {
        [_indices addObject:[NSNumber numberWithInt:i]];
    }
}

@end
