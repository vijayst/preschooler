//
//  ColorViewController.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 26/01/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ColorViewController.h"
#import "ViewController.h"

@interface ColorViewController ()
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) AnimationHelper *animHelper;
@property (strong, nonatomic) TTSHelper *ttsHelper;
-(void)showLandscape;
-(void)showPortrait;
@end

@implementation ColorViewController
{
    NSArray *_buttons;
    NSArray *_choices;
    BOOL _answered;
    BOOL _over;
}


@synthesize animHelper;
@synthesize audioHelper;
@synthesize ttsHelper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    audioHelper = [[AudioHelper alloc] init];
    audioHelper.delegate = self;
    animHelper = [[AnimationHelper alloc] init];
    animHelper.delegate = self;
    ttsHelper = [[TTSHelper alloc] init];
    ttsHelper.delegate = self;
    
    [self initButtons];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self.homeButton setTitle:@"" forState:UIControlStateNormal];
    
    self.choice1Button.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
    self.choice2Button.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
    self.choice3Button.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.5];
    self.choice4Button.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0 alpha:0.8];
    self.choice5Button.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    self.choice6Button.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(!self.game.paused)
    {
        self.view.hidden = YES;
        [self newGame];
    }
    else
    {
        [self refreshView];
        self.game.paused = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.hidden = NO;
    [self animate];

}

-(void)initButtons
{
    _buttons = @[self.choice1Button, self.choice2Button, self.choice3Button,
                 self.choice4Button, self.choice5Button, self.choice6Button];
    
    for(UIButton *button in _buttons) {
        [button setTitle:@"" forState:UIControlStateNormal];
    }
}


-(void)newGame
{
    _over = NO;
    [self.game reset];
    [self newQuiz];
    [self animate];
}

-(void)newQuiz
{
    [self.game getQuiz];
    [self refreshView];
}

-(void)refreshView
{
    [self.progressBar setProgress:self.game.progress / (float)self.game.total];
    
    for(UIButton *button in _buttons)
    {
        switch(self.game.gameType)
        {
            case GTColors:
            {
                ColorChoice *colorChoice = [self.game.quiz.choices objectAtIndex:button.tag];
                button.backgroundColor = colorChoice.color;
                [button setTitle:@"" forState:UIControlStateNormal];
                [button setImage:nil forState:UIControlStateNormal];
                break;
            }
            case GTShapes:
            case GTObjects:
            {
                ImageChoice *imageChoice = [self.game.quiz.choices objectAtIndex:button.tag];
                [button setTitle:@"" forState:UIControlStateNormal];
                [button setImage:imageChoice.image forState:UIControlStateNormal];
                [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
                button.backgroundColor = [UIColor colorWithRed:172/255.0 green:209/255.0 blue:233/255.0 alpha:1];
                break;
            }
            case GTBigLetters:
            case GTSmallLetters:
            case GTNumbers:
            {
                WordChoice *wordChoice = [self.game.quiz.choices objectAtIndex:button.tag];
                [button setTitle:wordChoice.word forState:UIControlStateNormal];
                button.backgroundColor = [UIColor colorWithRed:0 green:128.0/255 blue:1 alpha:1];
                button.titleLabel.font = [UIFont systemFontOfSize:120.0F];
                [button setImage:nil forState:UIControlStateNormal];
                break;
            }
        }
    }
}

-(void)animate
{
    [audioHelper playGameLoadSound];
    self.view.userInteractionEnabled = NO;
    for(UIButton *button in _buttons)
    {
        button.enabled = NO;
        [animHelper shimmer:button];
    }
}

-(void)gameloadPlayed
{
    [ttsHelper speakPrompt:self.game.quiz.prompt];
    self.captionLabel.text = self.game.quiz.prompt;
}

-(void)promptCompleted
{
    self.view.userInteractionEnabled = YES;
    for(UIButton *button in _buttons)
    {
        button.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tap:(UIButton *)sender {
    self.view.userInteractionEnabled = false;
    Quiz *quiz = self.game.quiz;
    quiz.attempts++;
    if(sender.tag==quiz.answerPosition)
    {
        [audioHelper playMatchSound];
        self.game.progress++;
        int points = kChoiceCount - quiz.attempts;
        if(points > 0)
            self.game.points += points;
        [self.progressBar setProgress:self.game.progress/(float)self.game.total];
        _over = self.game.progress == self.game.total ? YES : NO;
        _answered = YES;
    }
    else
    {
        _answered = NO;
        [audioHelper playNoMatchSound];
    }
    switch(self.game.gameType)
    {
        case GTColors:
        {
            ColorChoice *choice = quiz.choices[sender.tag];
            [animHelper zoom:sender state:choice.name];
            break;
        }
        case GTShapes:
        case GTObjects:
        {
            ImageChoice *choice = quiz.choices[sender.tag];
            [animHelper zoom:sender state:choice.name];
            break;
        }
        case GTBigLetters:
        case GTSmallLetters:
        case GTNumbers:
        {
            WordChoice *choice = quiz.choices[sender.tag];
            [animHelper zoom:sender state:choice.name];
        }
    }
    
}

-(void)animationCompleted:(NSString *)state
{
    [ttsHelper speakAnswer:[NSString stringWithFormat:@"%@.", state]];
    self.view.userInteractionEnabled = true;
}

-(void)answerCompleted
{
    if(_over)
    {
        [self showAchievement];
    }
    else{
        if(_answered)
        {
            [self newQuiz];
            [self animate];
        }
    }
}

- (IBAction)close:(id)sender {
    self.game.paused = YES;
    [self.presentingViewController dismissViewControllerAnimated:YES
                                                      completion:nil];
}

- (IBAction)newGame:(id)sender {
    [self newGame];
}

- (IBAction)stopGame:(id)sender {
    self.game.progress = self.game.total;
    [self showAchievement];
}

-(void)showAchievement
{
    __block ViewController *controller = (ViewController *)self.presentingViewController;
    [controller dismissViewControllerAnimated:YES
                                   completion:^{
                                       [controller presentViewController:controller.achievementController
                                                                animated:YES
                                                              completion:nil];
                                   }];
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}

-(void)showPortrait
{
    int xmargin = 30;
    int ymargin = 30;
    int xspacing = 28;
    int yspacing = 24;
    int width = 340;
    int height = 250;
    int row1 = 64;
    int row2 = 30;
    int row3 = 486;
    
    CGRect homeFrame = CGRectMake(xmargin, ymargin, row1, row1);
    CGRect newFrame = CGRectMake(xmargin + row1 + 10, ymargin, row1, row1);
    CGRect stopFrame = CGRectMake(xmargin + 2*(row1 + 10), ymargin, row1, row1);
    CGRect captionFrame = CGRectMake(xmargin + 3*(row1+10), ymargin, row3, row1);
    CGRect progressFrame = CGRectMake(xmargin, ymargin + row1 + yspacing, 708, row2);
    
    self.homeButton.frame = homeFrame;
    self.gameButton.frame = newFrame;
    self.stopButton.frame = stopFrame;
    self.captionLabel.frame = captionFrame;
    self.progressBar.frame = progressFrame;
    
    for(UIButton *button in _buttons)
    {
        NSInteger q = button.tag / 2;
        NSInteger m = button.tag % 2;
        CGRect buttonFrame = CGRectMake(xmargin + (width + xspacing)*m,
                                        ymargin + row1 + row2 + 2*yspacing + q*(height + yspacing),
                                        width, height);
        button.frame = buttonFrame;
    }
}

-(void)showLandscape
{
    int xmargin = 30;
    int ymargin = 30;
    int xspacing = 32;
    int yspacing = 28;
    int width = 300;
    int height = 265;
    int row1 = 64;
    int row2 = 30;
    int row3 = 742;
    
    CGRect homeFrame = CGRectMake(xmargin, ymargin, row1, row1);
    CGRect newFrame = CGRectMake(xmargin + row1 + 10, ymargin, row1, row1);
    CGRect stopFrame = CGRectMake(xmargin + 2*(row1 + 10), ymargin, row1, row1);
    CGRect captionFrame = CGRectMake(xmargin + 3*(row1+10), ymargin, row3, row1);
    CGRect progressFrame = CGRectMake(xmargin, ymargin + row1 + yspacing, 964, row2);
    
    self.homeButton.frame = homeFrame;
    self.gameButton.frame = newFrame;
    self.stopButton.frame = stopFrame;
    self.captionLabel.frame = captionFrame;
    self.progressBar.frame = progressFrame;
    
    
    for(UIButton *button in _buttons)
    {
        NSInteger q = button.tag / 3;
        NSInteger m = button.tag % 3;
        CGRect buttonFrame = CGRectMake(xmargin + (width + xspacing)*m,
                                        ymargin + row1 + row2 + 2*yspacing + q*(height + yspacing),
                                        width, height);
        button.frame = buttonFrame;
    }

}

@end
