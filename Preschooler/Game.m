//
//  Game.m
//  Preschooler
//
//  Created by Vijay Thirugnanam on 28/11/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Game.h"

@implementation Game

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.points = 0;
        self.progress = 0;
    }
    return self;
}

-(BOOL)over
{
    return self.progress == self.total;
}

-(int)rank
{
    int rank = self.points / self.total;
    rank += self.points % self.total == 0 ? 0 : 1;
    return rank;
}

-(void)reset
{
    self.points = 0;
    self.progress = 0;
    [self getData];
}

-(void)getData
{
    
}

-(void)getQuiz
{
    self.quiz = [[Quiz alloc] init];
    
    int index = arc4random() % [_indices count];
    NSInteger rndIndex = [_indices[index] integerValue];
    [_indices removeObjectAtIndex:index];
    
    self.quiz.prompt = [NSString stringWithFormat:@"Where is %@?", _names[rndIndex]];
    
    NSMutableSet *indexes = [[NSMutableSet alloc] init];
    while([indexes count] < kChoiceCount - 1)
    {
        int anotherIndex = arc4random() % [_data count];
        if(anotherIndex != rndIndex)
        {
            [indexes addObject:[NSNumber numberWithInt:anotherIndex]];
        }
    }
    NSMutableArray *choices = [[indexes allObjects] mutableCopy];
    self.quiz.answerPosition = arc4random() % kChoiceCount;
    [choices insertObject:[NSNumber numberWithInteger:rndIndex]
                  atIndex:self.quiz.answerPosition];
    
    _choiceNumbers = choices;
}

@end
